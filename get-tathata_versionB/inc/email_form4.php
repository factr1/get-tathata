<section class="email-signup">
	<div class="row">
		<div class="medium-6 columns">
			<h3>Still not convinced?</h3>
			<p>Let us prove to you why Tathata works right now, with our free video giveaway.</p>
		</div>
		<div class="medium-6 columns">
			<form class="hide-for-touch">
				<input type="email" name="initial_email" id="initial_email" value placeholder="Email Address" >
				<br>
				<input type="submit" value="Access My Free Videos" data-remodal-target="emailform" href="#" onclick="getEmail()">
			</form>
			
			<form class="show-for-touch">
				<input type="submit" value="Enter Email and Access Free Videos" data-remodal-target="emailform" href="#">
			</form>
			
		</div>
	</div>
</section>


<div class="remodal" data-remodal-id="emailform">
  <button data-remodal-action="close" class="remodal-close"></button>
  <h1>Free Video Giveaway</h1>
  <p>
    Enter your e-mail for immediate access and <br>learn how Tathata Golf will help your game.
  </p>

<? 
	gravity_form( 4, false, false, false,'', false );
?>

<script>
	var userEmail;
	var getEmail = function(){
		userEmail = document.getElementById('initial_email').value;	
		jQuery('#input_4_2').val(userEmail);
	}
</script>

</div>