<section class="inside-the-program">
	<div class="row">
		<div class="medium-10 columns medium-centered text-center">
			<h1>What's Inside the Program?</h1>
		</div>
	</div>
	<div class="row">
		<div class="medium-6 columns">
			<p>
				<strong>Chapter Breakdown:</strong>
				<ul>
					<li><i class="fa fa-check-circle"></i> Chapter 1 | Days 1-10 | Body, Stretching & Mind 1</li>
					<li><i class="fa fa-check-circle"></i> Chapter 2 | Days 11-20 | Hands, Arms & Mind 2</li>
					
					<li><i class="fa fa-check-circle"></i> Chapter 3 | Days 21-30 | Pressure, Impact & Mind 3</li>
					<li><i class="fa fa-check-circle"></i> Chapter 4 | Days 31-40 | Speed, Strength & Mind 4</li>
					<li><i class="fa fa-check-circle"></i> Chapter 5 | Days 41-50 | Short Game, Putting & Mind 5</li>
					<li><i class="fa fa-check-circle"></i> Chapter 6 | Days 51-60 | Shape, Trajectory & Mind 6</li>
				</ul>

			</p>
		</div>
		<div class="medium-6 columns">
			<p>
			<strong>Course Takeaways: </strong>
			<ul>
			<li><i class="fa fa-plus-circle"></i> 140 training movements</li>
			<li><i class="fa fa-plus-circle"></i> 14 different movement routines</li>
			<li><i class="fa fa-plus-circle"></i> 3 greatest golfer and athletes movement videos</li>
			<li><i class="fa fa-plus-circle"></i> 195 test questions and video answers</li>
			<li><i class="fa fa-plus-circle"></i> 45 deeper discussions (mental and movement)</li>
			<li><i class="fa fa-plus-circle"></i> 13 mental training lay down exercises</li>
			<li><i class="fa fa-plus-circle"></i> 12 sitting/standing mental training exercises</li>
			</ul>

			</p>
		</div>
	</div>
</section>