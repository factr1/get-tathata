<article class="darkbar home-bg">
	<div class="row">
		<div class="small-centered medium-6 medium-uncentered columns">
			<h1>Fun, Easy and Convenient</h1>
			<h3>Enjoy the most powerful learning and training experience in the game of golf at your own pace, on your own schedule</h3>
			
			<ul>
				<li>Created for golfers of all ages, body types and ability levels</li>
				<li>Finish the program in 30 days, 60 days, 120 days, 6 months or 12 months, it is completely up to you</li>
				<li>Learn and train for just 45-70 minutes per day from the comfort of your own home, at the course, at the gym, on the road or anywhere else you choose </li>
				<li>Improve YEAR-ROUND regardless of climate and weather conditions</li>
				<li>Enjoy the product in full HD 1080p on all devices - computer, smart phone, tablet, TV</li>
				<li>30-Disc DVD version available as purchase add-on</li>
			</ul>
		</div>
	</div>
</article>