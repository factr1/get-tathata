<section class="sevenday_trial_cta">
	<div class="row">
		<div class="medium-4 columns">
			<a href="http://tathatagolf.com/coupon/7daytrial/<?php echo $affiliate; echo $urlvars; ?>" class="bigButton">
				<p style="padding: 10px 0 5px;">60-Day Training Program</p>
				<h4>FREE 7-Day Trial</h4>
				<p>Click Here To Start</p>
			</a>
		</div>
		<div class="medium-8 columns">
			<p>Enjoy a free 7-Day trial of the most revolutionary golf training program ever developed. 
			This is the world’s first 60-Day In-Home Golf Training Program ever created and one of the most expansive and 
			thorough learning and training programs the golf industry has ever seen.</p>

			<p><span class="red">Risk free! No credit cards, no catch, 100% access!</span></p>
		</div>
		
	</div>
</section>