<section class="message-from">
	<div class="row">
		<div class="medium-10 medium-centered text-center columns">
			<h1>A Message From Gary & Brandel</h1>
		</div>
		<div class="medium-8 columns">
			<div class="flex-video vimeo widescreen">
			<iframe src="https://player.vimeo.com/video/134999203?color=ffffff&title=0&byline=0&portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
		</div>
		<div class="medium-4 columns">
			<?php include('redbox.php');?>
			<br>
			<?php if(have_rows('partner_logos')):?>
				<ul class="small-block-grid-1 medium-block-grid-2">
					<?php while(have_rows('partner_logos')): the_row();?>
					<li>
						[partner logo]
					</li>
					<?php endwhile;?>
				</ul>
			<?php endif;?>
		</div>
	</div>
</section>