<section class="movement-training">
	<div class="row">
		<div class="medium-6 columns">
			<h1 class="meditate-title">Tathata Movement Training</h1>
			<h3>The new type of training that is revolutionizing the way golf is learned and played</h3>
		</div>
		<div class="medium-6 columns movement-list">
			<ul>
				<li>Learn, improve and retain information at a pace never before seen in golf</li>
				<li>Build PRECISION and POWER not only into your ball flight but also into your movement/swing</li>
				<li>Train and build your entire game with several different easy to perform, yet extremely detailed movement routines</li>
				<li>For the first time ever, learn not only WHAT to do but WHY and HOW to do it</li>
				<li>Utilize stretching routines to support your movement training and full swing motion while helping to eliminate constantly nagging aches and pains</li>
			</ul>
		</div>
	</div>
</section>