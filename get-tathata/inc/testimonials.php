<section class="testimonials toast">
	<div class="row">
		<div class="medium-10 columns medium-centered text-center">
			<h1>What are students saying?</h1>
		</div>
	</div>
	
	<div class="row single">
	<div class="small-4 medium-2 columns"><img src="<?php bloginfo('template_url'); ?>/images/testimonials/KarenDavies2.jpg"></div>
				<div class="small-8  medium-10 columns">
						<blockquote>"Tathata has given me a new way to look at how I play and teach the game and excited me to do more than I have ever done. My game has changed. I have more power, more presence and more enthusiasm. I get up in the morning and I want to go do it. That hasn’t happened for more than a decade."</blockquote>
						<cite>Karen Davies, LPGA, 2010 LPGA T&CP National Champion</cite>
				</div>
	</div>
	
	<div class="row single">
	<div class="small-4 medium-2 columns medium-push-10"><img src="<?php bloginfo('template_url'); ?>/images/testimonials/PaxtonJevnick.png"></div>
			<div class="small-8  medium-10 medium-pull-2 columns">
				<blockquote>"It's not a bunch of swing tips, its not a bunch of bandaids, it's built from the ground up, the right way, and it’s going to last for me, it's not going to disappear tomorrow, it’s in me."</blockquote>
				<cite>Paxton Jevnick, HDCP 12, Tathata Student</cite>
			</div>
	</div>
	
	<div class="row single">
		<div class="small-4 medium-2 columns"><img src="<?php bloginfo('template_url'); ?>/images/testimonials/Todd-Demsey.jpg"></div>
			<div class="small-8  medium-10 columns">
				<blockquote>"With Bryan and Tathata, it just becomes a part of you, golf becomes fun again. You do the training and you're ready to play."</blockquote>
				<cite>Todd Demsey, Tour Professional, 4-Time All-American</cite>
		</div>
	</div>
	
	<div class="row single">
		<div class="small-4 medium-2 medium-push-10 columns"><img src="<?php bloginfo('template_url'); ?>/images/testimonials/DianeMott.png"></div>
			<div class="small-8  medium-10 medium-pull-2 columns">
				<blockquote>"Tathata Golf has definitely caused me to enjoy golf more and look forward to playing, look forward to shots that otherwise I might have feared."</blockquote>
				<cite>Diane Mott, HDCP 12, Tathata Student</cite>
		</div>
	</div>
	
	<div class="row single">
		<div class="small-4 medium-2 columns"><img src="<?php bloginfo('template_url'); ?>/images/testimonials/Rob-Mangini.jpg"></div>
			<div class="small-8  medium-10 columns">
				<blockquote>"I've been fortunate to have worked with some of the world's top-rated instructors. Tathata Golf is definitely on an entirely different learning level."</blockquote>
				<cite>Rob Mangini, HDCP +1, Tathata Student</cite>
		</div>
	</div>
	
	<div class="row single">
		<div class="small-4 medium-2 medium-push-10 columns"><img src="<?php bloginfo('template_url'); ?>/images/testimonials/Alan-Jackson.jpg"></div>
			<div class="small-8  medium-10 medium-pull-2 columns">
				<blockquote>"I’ve never tried anything that compares to Tathata training. I make better contact, I’m hitting it longer, I just have more enthusiasm for the game."</blockquote>
				<cite>Allen Jackson, HDCP 15, Tathata Student</cite>
			</div>
	</div>



	</div>
</section>