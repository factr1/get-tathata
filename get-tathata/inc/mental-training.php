<section class="mental-training">
	<div class="row">
		<div class="medium-7 columns">
			<h1>Tathata Mental Training</h1>
			<h3>Move far beyond the beginner’s mind of modern sports psychology </h3>
			<ul>
				<li>Discover 2,500-year-old martial art mental training truths combined with a delivery founded in 
				neuro-linguistic programming to experience an uncommon sense of greatness</li>
				<li>Experience the life-changing power that comes from becoming absolutely clear with “who you are” and “where you are going”</li>
				<li>Learn the benefits of training your spontaneous reactions and first instincts on and off the golf course</li>
				<li>Move past suffering and come to embody the strength of a samurai</li>
				<li>Learn how to shoot the lowest score of your life and keep doing it time after time </li>
			</ul>
		</div>
	</div>
</section>



<!--
<article class="meditate">
	<div class="row">
	<h1>Tathata Mental Training</h1>
	<h3>Move far beyond the beginner’s mind of modern sports psychology </h3>
	
	
	<div class="row">
		<div class="medium-6 columns">
			<ul>
				<li>Discover 2,500-year-old martial art mental training truths combined with a delivery founded in 
				neuro-linguistic programming to experience an uncommon sense of greatness</li>
				<li>Experience the life-changing power that comes from becoming absolutely clear with “who you are” and “where you are going”</li>
			</ul>
		</div>
		
		<div class="medium-6 columns">
			<ul>
				<li>Learn the benefits of training your spontaneous reactions and first instincts on and off the golf course</li>
				<li>Move past suffering and come to embody the strength of a samurai</li>
				<li>Learn how to shoot the lowest score of your life and keep doing it time after time </li>
			</ul>
		</div>
	</div>
-->
	
	<!-- Section 4 special note -->
<!--
	<div class="row">
		<center><div class="medium-9 medium-centered columns highlight">	
			<h1 class="highlight-text">Once your training changes, thoughts and outcomes immediately begin to change.</h1>
		</div></center>
	</div>

	</div>
</article>
-->
