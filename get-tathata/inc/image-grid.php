<section class="toast">
	<div class="row gridintro">
		<div class="medium-9 medium-centered text-center columns">
			<h1>What Makes Tathata Unique?</h1>
			<p class="show-for-touch">Tap on each photo to see more</p>
		</div>
	</div>
</section>

<section class="image-grid">
	<div class="row">
		<?php if(have_rows('image_grid', 27)):?>
		<ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
			<?php while(have_rows('image_grid', 27)): the_row();?>
			<li>
				<div class="image-grid-container" style="background: url(<?php the_sub_field('image_grid_image');?>) top center no-repeat;">
					<div class="box-overlay">
						<p class="text-center"><?php the_sub_field('grid_content');?></p>
					</div>
					<p class="text-center grid-title"><?php the_sub_field('grid_title');?></p>
				</div>
			</li>
			<?php endwhile;?>
		</ul>
		<?php endif;?>
	</div>
</section>