<section class="instant-access">
	<div class="row">
		<div class="medium-8 columns">
			<img src="<?php bloginfo('template_url'); ?>/images/Online_instantAccess_devices.jpg" alt="Online_instantAccess_devices" width="1000" height="600" />
		</div>
		<div class="medium-4 columns">
			<h2>Train Anywhere</h2>
			<p>Enjoy our streaming product on any device wherever you may be.</p>
			<?php include('redbox.php');?>
		</div>
	</div>
</section>