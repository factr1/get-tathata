<!-- Sub page copy.  -->
<article class="subpage">

<!-- Section 1 -->
<?php include('fun-and-easy.php');?>

<!-- Section 2 -->
<?php include('movement-training.php');?>
	
	

<!-- ======   Section 3  ========= --> 

<?php include('cta.php');?>



<!-- Section 4 -->
<?php include('mental-training.php');?>



<!-- ======   Section 5  ========= --> 


<!-- Expanded Testimonials -->
<?php include('testimonials-expanded.php');?>



<!-- Section 16 -->
<section class="">
	<div class="row">
	<h1 class="red">A Complete Learning and Training Curriculum</h1>
	<h3>The world’s most thorough and complete mind, body and swing training curriculum ever created for golfers of all ages, body types and ability levels. </h3>
	</div>
	
	<div class="image-row">
		<img class="curriculum" src="http://gettathata.com/wp-content/uploads/2015/08/Streaming-ChapterOverview.png.png" alt="Streaming-ChapterOverview">
	</div>
	
	<div class="row">
		<div class="medium-6 columns">
			<p>Tathata students quickly realize they are capable of much greater learning and retention while being guided through a strategically built and structured path of learning than what is currently seen in golf instruction. For the first time ever, students are discovering not only how they can improve in their games, but also come to understand all the things they already do well in their swing and dozens of complimentary movements to add to their natural efficiencies.</p>
		
			<p>With the most transformational and empowering learning and training experience ever offered in the game of golf, you are just 2 months away from playing golf at a level far beyond anything you have ever thought possible and never looking back. 
			</p>
		</div>

		<div class="medium-6 columns">
			<h3 class="red">Build/enhance the following golf motions:</h3>
				<div class="row">
					<div class="small-6 columns">
						<ul>
							<li>Full Swing</li>
							<li>Pitching</li>
							<li>Chipping</li>
							<li>Flop/lob shots</li>
						</ul>
					</div>
					
					<div class="small-6 columns">
						<ul>
							<li>Bunker shots</li>
							<li>Putting</li>
							<li>All shapes</li>
							<li>All trajectories</li>
						</ul>
					</div>
				</div>
		</div>

	</div>
</section>


</article>
