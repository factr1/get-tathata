<section class="hide-from-print">	
		<div class="row">
		    <div class="medium-11 columns medium-centered">
		        <img src="<?php bloginfo('template_url');?>/images/brandlogos2.png" alt="brandlogos" width="927" height="108" />
		    </div>
		</div>	
		<footer>
		    <div class="row">
		        <div class="medium-4 columns">
			       <h3>Quick Links</h3>
			       <div> 
				       <?php wp_nav_menu(array('theme_location' => 'quicklinks'));?>
			       </div>
		        </div>
		        <div class="medium-4 columns">
			        <a href="http://gettathata.com/become-an-affiliate/" target="_blank">
				        <img src="<?php bloginfo('template_url');?>/images/affiliate-new.png">
			        </a>
		        </div>
		        <div class="medium-4 columns shareUs">
			        <h3>Share & Explore</h3>
	                <div>
	                    <p><span>share us:</span>
							<a href="javascript:void(0)" class="icon-share addthis_button_compact"></i></a>
						</p>
	                    <p>Where else to find us:</p>
	                    <p>                    
	                        <a target="_blank" href="https://www.facebook.com/pages/Tathata-Golf/526700354108550" class="icon-fb"></i></a>
	                        <!-- <a href="javascript:void(0)" class="icon-gP"></i></a> -->
	                        <a target="_blank" href="https://twitter.com/TathataGolf" class="icon-tw"></i></a>
	                        <a target="_blank" href="https://www.linkedin.com/in/tathatagolf" class="icon-in"></i></a>
	                        <a target="_blank" href="https://i.instagram.com/tathatagolf" class="icon-pin"></i></a>
	                        <!-- <a href="javascript:void(0)" class="icon-vi"></i></a> -->
	                        <a target="_blank" href="https://www.youtube.com/channel/UCG1AlyrH_eqgQ7cekyYc3Ig" class="icon-yt"></i></a>                    
	                    </p>
	                </div>
		        </div>
		    </div>
		    <div class="footer-copyright">
			    <div class="row">
				    <div class="medium-10 columns">
					    <p><img src="<?php bloginfo('template_url');?>/images/tiny-logo.png" alt="Small Tathata logo">&copy; <?php echo date('Y');?> Tathata, LLC &copy; 2012-15 Hepler Golf, LLC. All Rights Reserved. <a href="http://tathatagolf.com/privacy-policy/">Privacy Policy</a> / <a href="http://tathatagolf.com/terms/">Terms of Use</a> / <a href="http://tathatagolf.com/returns-and-refunds/">Return Policy</a><br>No imagery or logos contained within this site may be used without the express permission of Hepler Golf, LLC.</p>
				    </div>
			    </div>
		    </div>
		</footer>
</section>

<?php wp_footer(); ?>

<script src="<?php bloginfo('template_url');?>/js/newToggles.js"></script>
</body>
</html>
<?php
ob_end_flush();
flush();
ob_end_clean();