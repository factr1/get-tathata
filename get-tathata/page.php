<?php get_header(); ?>

<article class="row">
<div class="medium-10 medium-centered columns">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<div class="post" id="post-<?php the_ID(); ?>">
			<h1><?php //the_title(); ?></h1>
			<?php the_content(); ?>
			<?php edit_post_link('Edit this entry.', '<hr><p>', '</p>'); ?>
			
			</div>
		<?php endwhile; endif; ?>
</div>
</article>
			
<?php if(is_page('155')): // Set up better content for Affiliate welcome page ?>
	<div class="row">
		<div class="medium-10 medium-centered columns">
			<div class="flex-video vimeo widescreen">
				<iframe src="https://player.vimeo.com/video/139493571?autoplay=0&color=ffffff&title=0&byline=0&portrait=0" width="500" height="281" 
				frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
		</div>
	</div>
	
	<div class="darkbar">
		<div class="row">
			<div class="medium-10 medium-centered columns ">
				<h3>3 Steps to Complete your Affiliate Set up</h3>
				<p>Step 1: <a href="http://gettathata.com/wp-content/uploads/2015/11/AffiliatePacket-TathataGolf.pdf">Download the affiliate packet</a></p>
				<p>Step 2: <a href="http://gettathata.com/affiliate/contract/" target="_blank">Review contract terms</a></p>
				<p>Step 3: Fill out the form below</p>
			</div>
		</div>
	</div>
	
	
	<div class="row">
		<div class="medium-10 medium-centered columns">
		<?php gravity_form( 3, false, false, false, '', false ); ?>
		</div>
	</div>




<?php endif; ?>



		
		


<?php get_footer(); ?>
