<?php /* Template Name: Email Page */
	get_header();
	$i = 0;
?>

	
<!-- First Section -->
<section class="top-block hide-for-small-only">
	<div class="row">
		<div class="medium-10 columns medium-centered text-center">
			<h1>Welcome to Your FREE Video Giveaway</h1>
			<h2>Enjoy these exclusive videos from Tathata Golf</h2>
		</div>
		
		<!-- video container -->
		<?php if(have_rows('videos')):?>
			<?php while(have_rows('videos')): the_row(); $i++;?>
			<div class="video-container video-container-<?php echo $i;?>">
				
					<div class="medium-4 columns">
						<h3><?php the_sub_field('video_title');?></h3>
						<p>
							<?php the_sub_field('video_description');?>
						</p>
						<p>
							<?php the_sub_field('total_time');?>
						</p>
					</div>
					<div class="medium-8 text-center columns">
						<div class="flex-video vimeo widescreen">
							<?php the_sub_field('video_embed');?>
						</div>
					</div>
				
			</div>
			<?php endwhile;?>
		<?php endif;?>
		<!-- end video container-->
		
	</div>
	
	<?php include('inc/as-seen-on.php');?>
	
	<!-- Video Selection -->
<div class="video-selection hide-for-small-only">
<p class="videobreak show-for-small-only">Additional Videos</p>
	<?php if(have_rows('videos')): $i = 0;?>
		<div class="row">
			<?php while(have_rows('videos')): the_row(); $i++;?>
				<div class="medium-4 columns video-select-<?php echo $i;?>">
					<div class="flex-video  vimeo widescreen">
						<?php the_sub_field('video_embed');?>
					</div>
					<p class="video-title">
						<?php the_sub_field('video_title');?>
					</p>
					<p>
						<?php the_sub_field('total_time');?>
					</p>
				</div>
			<?php endwhile;?>
		</div>
	<?php endif;?>
</div>
</section>

<!-- Mobile Video Sections -->
<section class="top-block-mobile show-for-small-only">
	<div class="row">
		<div class="small-12 columns text-center">
			<h1>Welcome to Your FREE Video Giveaway</h1>
			<h2>Enjoy these exclusive videos from Tathata Golf</h2>
		</div>
	</div>
	<div class="row">
		<!-- mobile video container -->
		<?php if(have_rows('videos')):?>
			<?php while(have_rows('videos')): the_row(); $i++;?>
			<div class="video-container video-container-<?php echo $i;?>">
				
					<div class="small-12 text-center columns">
						<h3 class="mobile-header"><?php the_sub_field('video_title');?></h3>
						
						<div class="flex-video vimeo widescreen">
							<?php the_sub_field('video_embed');?>
						</div>
					</div>
					<div class="small-12 columns">
						
						<p>
							<?php the_sub_field('video_description');?>
						</p>
						<p>
							<?php the_sub_field('total_time');?>
						</p>
					</div>
				
			</div>
			<?php endwhile;?>
		<?php endif;?>
		<!-- end mobile video container-->
	</div>
</section>

<div id="buynow"></div>
<!-- CTA Section / Block -->
<?php include('inc/cta.php');?>


<!-- Instant Access Block -->
<?php include('inc/instant-access.php');?>








<!-- ======   Section 2  ========= -->  

<!-- Image Grid -->
<?php include('inc/image-grid.php');?>


<!-- ======   Section 3  ========= --> 

<div class="alt-color">
<?php include('inc/cta.php');?>
</div>

<!-- ======   Section  4 ========= --> 

<!-- Whats Inside the program? -->
<?php include('inc/inside-the-program.php');?>

<!-- ======   Section 5  ========= --> 

<!-- Message from Gary & Brandel -->
<?php include('inc/message-from.php');?>


<!-- New Email Sections -->
<?php include('inc/sub-page.php');?>


<!-- ======   Section  17  ========= --> 

<section class="cta">
	<div class="row">
		<div class="medium-8 columns">
			<p class="cta_headline">
			"If you’re ready to <span>get better fast</span>, your search is over,<br>this is it, this is the way."
			<span> &mdash; Gary McCord</span>

			</p>
		</div>
		<div class="medium-4 columns">
			<?php include('inc/redbox.php');?>		 	
		</div>
	</div>
</section>




<!-- ======   Section 20 ========= --> 

<?php get_footer();?>


