<?php /* Template Name: Home Page */
	get_header();
?>

<section class="home-top">
	<div class="row">
		<div class="medium-9 medium-centered text-center columns">
			<h1>Experience golf's most <span>transformational</span> and <span>empowering</span> training opportunity ever introduced to the game!</h1>
		</div>
		<div class="medium-12 columns text-center big-video">
			<div class="flex-video vimeo widescreen">
				<iframe src="https://player.vimeo.com/video/137528720?autoplay=1&color=ffffff&title=0&byline=0&portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
		</div>
	</div>
	
	
			
			
</section>
<!-- Email Signup -->
<?php include('inc/email2.php');?>

<!-- Product Buy  Block -->
<?php include('inc/product-buy.php');?>

<!-- Image Grid -->
<?php include('inc/image-grid.php');?>

<!-- Instant Access Block -->
<?php include('inc/instant-access.php');?>

<!-- Whats Inside the program? -->
<?php include('inc/inside-the-program.php');?>

<!-- Email Signup -->
<?php include('inc/email.php');?>

<!-- Message from Gary & Brandel -->
<?php include('inc/message-from.php');?>

<!-- Testimonials-->
<?php include('inc/testimonials.php');?>

<!-- REPEAT CTA Section / Block -->
<div class="alt-color">
<section class="cta">
	<div class="row">
		<div class="medium-8 columns">
			<p class="cta_headline">
			"If you’re ready to <span>get better fast</span>, your search is over,<br>this is it, this is the way."
			<span> &mdash; Gary McCord</span>

			</p>
		</div>
		<div class="medium-4 columns">
			<?php include('inc/redbox.php');?>		
		</div>
	</div>
</section>
</div>


<?php get_footer();?>