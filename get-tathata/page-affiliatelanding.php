<?php /* Template Name: Affiliate Home Page */
	get_header();
?>


<?
//  All ACF options
//  affiliate_name
//  affiliate_id
//  affiliate_logo
//  affiliate_introduction
?>

<section class="home-top versionb">
	<div class="row">
		<div class="medium-9 medium-centered text-center columns">
			<?php if(get_field('affiliate_introduction')):
					the_field('affiliate_introduction');
				else:?>
			<h1>Hailed By Top Golf Instructors & Analysts
As The Most <span>Revolutionary</span> & <span>Refreshing</span> Golf Training Ever Created!</h1>
			<?php endif;?>
		</div>
	</div>
	
	
	<!-- First Section -->
<section class="top-block hide-for-small-only">
	<div class="row">

		
		<!-- video container -->
		<?php if(have_rows('videos', 172)):?>
			<?php while(have_rows('videos', 172)): the_row(); $i++;?>
			<div class="video-container video-container-<?php echo $i;?>">
				
					<div class="medium-4 columns">
						<h3><?php the_sub_field('video_title', 172);?></h3>
						<p>
							<?php the_sub_field('video_description', 172);?>
						</p>
						<p>
							<?php the_sub_field('total_time', 172);?>
						</p>
					</div>
					<div class="medium-8 text-center columns">
						<div class="flex-video vimeo widescreen">
							<?php the_sub_field('video_embed', 172);?>
						</div>
					</div>
				
			</div>
			<?php endwhile;?>
		<?php endif;?>
		<!-- end video container-->
		
	</div>
	
	<?php include('inc/as-seen-on.php');?>
	
	<!-- Video Selection -->
<div class="video-selection hide-for-small-only">
<p class="videobreak show-for-small-only">Additional Videos</p>
	<?php if(have_rows('videos', 172)): $i = 0;?>
		<div class="row">
			<?php while(have_rows('videos', 172)): the_row(); $i++;?>
				<div class="medium-4 columns video-select-<?php echo $i;?>">
					<div class="flex-video  vimeo widescreen">
						<?php the_sub_field('video_embed', 172);?>
					</div>
					<p class="video-title">
						<?php the_sub_field('video_title', 172);?>
					</p>
					<p>
						<?php the_sub_field('total_time', 172);?>
					</p>
				</div>
			<?php endwhile;?>
		</div>
	<?php endif;?>
</div>
</section>

<!-- Mobile Video Sections -->
<section class="top-block-mobile show-for-small-only">
	<div class="row">
		<!-- mobile video container -->
		<?php if(have_rows('videos', 172)):?>
			<?php while(have_rows('videos', 172)): the_row(); $i++;?>
			<div class="video-container video-container-<?php echo $i;?>">
				
					<div class="small-12 text-center columns">
						<h3 class="mobile-header"><?php the_sub_field('video_title', 172);?></h3>
						
						<div class="flex-video vimeo widescreen">
							<?php the_sub_field('video_embed', 172);?>
						</div>
					</div>
					<div class="small-12 columns">
						
						<p>
							<?php the_sub_field('video_description', 172);?>
						</p>
						<p>
							<?php the_sub_field('total_time', 172);?>
						</p>
					</div>
				
			</div>
			<?php endwhile;?>
		<?php endif;?>
		<!-- end mobile video container-->
	</div>
</section>




		
</section>
<!-- Email Signup -->
<?php include('inc/email2.php');?>


<!-- Product Buy  Block -->
<?php include('inc/product-buy.php');?>

<!-- Image Grid -->
<?php include('inc/image-grid.php');?>

<!-- Instant Access Block -->
<?php include('inc/instant-access.php');?>

<!-- Whats Inside the program? -->
<?php include('inc/inside-the-program.php');?>

<!-- Email Signup -->
<?php include('inc/email.php');?>

<!-- Message from Gary & Brandel -->
<?php include('inc/message-from.php');?>

<!-- Fun, Easy and Convenient-->
<?php include('inc/fun-and-easy.php');?>

<!-- Tathata Movement Training -->
<?php include('inc/movement-training.php');?>

<!-- CTA -->
<?php include('inc/cta.php');?>

<!-- Tathata Mental Training -->
<?php include('inc/mental-training.php');?>

<!-- Testimonials-->
<?php include('inc/testimonials.php');?>

<!-- Section 16 -->
<article class="">
	<div class="row">
	
		<div class="medium-6 columns">
			<h1 class="red" style="padding-top: 40px;">A Complete Learning and Training Curriculum</h1>
			<h3 style="padding:40px 0 50px">The world’s most thorough and complete mind, body and swing training curriculum ever created for golfers of all ages, body types and ability levels. </h3>
		</div>
		
		<div class="medium-6 columns">
			<img class="curriculum" src="http://gettathata.com/wp-content/uploads/2015/08/Streaming-ChapterOverview.png.png" alt="Streaming-ChapterOverview">
		</div>
	
	</div>
	
	
	<div class="row">
		<div class="medium-6 columns">
			<p>Tathata students quickly realize they are capable of much greater learning and retention while being guided through a strategically built and structured path of learning than what is currently seen in golf instruction. For the first time ever, students are discovering not only how they can improve in their games, but also come to understand all the things they already do well in their swing and dozens of complimentary movements to add to their natural efficiencies.</p>
		
			<p>With the most transformational and empowering learning and training experience ever offered in the game of golf, you are just 2 months away from playing golf at a level far beyond anything you have ever thought possible and never looking back. 
			</p>
		</div>

		<div class="medium-6 columns">
			<h3 class="red">Build/enhance the following golf motions:</h3>
				<div class="row">
					<div class="small-6 columns">
						<ul>
							<li>Full Swing</li>
							<li>Pitching</li>
							<li>Chipping</li>
							<li>Flop/lob shots</li>
						</ul>
					</div>
					
					<div class="small-6 columns">
						<ul>
							<li>Bunker shots</li>
							<li>Putting</li>
							<li>All shapes</li>
							<li>All trajectories</li>
						</ul>
					</div>
				</div>
		</div>

	</div>
</article>


<div class="row" style="padding:50px 0;">
	<div class="medium-6 columns medium-centered">
		<h2 class="text-center">Learn more at <a href="http://TathataGolf.com/<?php echo $affiliate; echo $urlvars; ?>">TathataGolf.com</a></h2>
	</div>
</div>

<section class="hide-for-small">
<?php putRevSlider("home-page"); ?>
</section>


<!-- REPEAT CTA Section / Block -->
<section class="cta">
	<div class="row">
		<div class="medium-8 columns">
			<p class="cta_headline">
			"If you’re ready to <span>get better fast</span>, your search is over,<br>this is it, this is the way."
			<span> &mdash; Gary McCord</span>

			</p>
		</div>
		<div class="medium-4 columns">
		
			<?php include('inc/redbox.php');?>		 	
		</div>
	</div>
</section>


<?php get_footer();?>