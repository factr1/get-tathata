<?php
ob_start();
?>
<!doctype html>

<!--[if lt IE 7 ]> <html lang="en" class="no-js ie ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie ie9"> <![endif]-->
<!--[if gt IE 9]>  <html lang="en" class="no-js ie">     <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js">    <!--<![endif]-->


<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">


    <?php if (is_search()) { ?>
       <meta name="robots" content="noindex, nofollow" />
    <?php } ?>


<meta property="og:title" content="<?php the_title(); ?>" />
<meta property="og:site_name" content="<?php bloginfo('name') ?>">

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>


<?php wp_head(); ?>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/css/normalize.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/css/foundation.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/print.css" type="text/css" media="print">

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/remodal.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/remodal-default-theme.css">
<script src="<?php bloginfo('template_url'); ?>/js/remodal.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js">


<!--[if IE 8]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/modernizer.js"></script>
<![endif]-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60684002-3', 'auto');
  ga('send', 'pageview');

</script>

<!-- AddThis script -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54f23b0841238168" async="async"></script>

</head>

<body <?php body_class(); ?>>

    <header class="hide-from-print">
        
            <?php if(is_page_template('page-affiliatelanding.php')):?>
           <div class="row affiliaterow">
                <div class="small-12 small-text-center medium-6 columns medium-text-left">
                	<div class="logohold">
                    <img src="<?php bloginfo('template_url');?>/images/logo-1.png" alt="Tathata Golf logo">
                	</div>
                </div>
                <div class="small-12 small-text-center medium-6 columns medium-text-right">
                <?php if(get_field('affiliate_logo')): ?>
                    <img src="<?php the_field('affiliate_logo');?>" alt="Affiliate logo" class="affiliatelogo">
                 <?php endif; ?>
                </div>
           </div>
            <?php else:?>
            <div class="row">
            <div class="small-12 medium-6 medium-centered text-center columns">
                <img src="<?php bloginfo('template_url');?>/images/logo-1.png" alt="Tathata Golf logo">
            </div>
            </div>
            <?php endif;?>
    </header>
    
    
    <header class="show-for-print">
    	<img src="<?php bloginfo('template_url');?>/images/logo-print.jpg" alt="Tathata Golf logo">
    </header>
