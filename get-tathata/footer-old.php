<section class="hide-from-print">	
		<div class="row">
		    <div class="medium-8 columns medium-centered">
		        <img src="<?php bloginfo('template_url');?>/images/brandlogos.png" alt="brandlogos" width="927" height="108" />
		    </div>
		</div>	
		<footer>
		    <div class="row">
		        <div class="medium-5 columns">
		            <img src="<?php bloginfo('template_url');?>/images/logo-1.png" alt="Tathata Golf logo">
		        </div>
		        <div class="medium-7 text-right columns">
		            <p>
		                &copy; <?php echo date('Y');?> Tathata Golf  - <a href="http://www.tathatagolf.com/?utm_source=gettathata&utm_medium=footer">Visit full site</a>&nbsp;|&nbsp;
		                 <a href="http://gettathata.com/become-an-affiliate/">Become an Affiliate</a>
		            </p>
		        </div>
		    </div>
		</footer>
</section>

<?php wp_footer(); ?>

<script src="<?php bloginfo('template_url');?>/js/newToggles.js"></script>
</body>
</html>
<?php
ob_end_flush();
flush();
ob_end_clean();